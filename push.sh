#!/bin/sh
yarn build
zip -r welfare-admin.zip dist/*
scp welfare-admin.zip notice:/data/web_root
ssh notice < pull.sh