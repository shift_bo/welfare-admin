import axios from 'axios'
import { Message } from 'iview'
// oa-notice.17zuoye.net
console.log(process.env.VUE_APP_BASE_URL)
const request = axios.create({
  baseURL: process.env.VUE_APP_BASE_URL + '/welfare/w',
  timeout: 18000
})

request.interceptors.response.use((response) => {
  if (response.data.status !== 200) {
    console.log(response)
    Message.error(response.data.errmsg)
  } else {
    return response
  }
})

export default request
