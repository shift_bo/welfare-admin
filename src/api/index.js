import request from './request'

export const addOrigin = (data) => {
  return request({
    url: '/origin',
    method: 'post',
    data
  })
}

export const addRange = (data) => {
  return request({
    url: '/range',
    method: 'post',
    data
  })
}

export const getRange = () => {
  return request({
    url: '/origin',
    method: 'get'
  })
}

export const addWelfare = (data) => {
  return request({
    url: '/welfare',
    method: 'post',
    data
  })
}

export const addSendWelfare = (data) => {
  return request({
    url: '/sendwelfare',
    method: 'post',
    data
  })
}

export const getWelfare = (data) => {
  return request({
    url: '/welfare',
    method: 'get'
  })
}
export const getWelfareDetail = (welfare_id) => {
  return request({
    url: `/welfaredetail?welfare_id=${welfare_id}`,
    method: 'get'
  })
}

export const getWelfareQRcode = (welfare_id) => {
  return request({
    url: `/welfareqrcode?welfare_id=${welfare_id}`,
    method: 'get'
  })
}

export const getWelfareGetDetail = (type, page, s) => {
  return request({
    url: `/welfaregetdetail?type=${type}&page=${page}&s=${s}`,
    method: 'get'
  })
}
